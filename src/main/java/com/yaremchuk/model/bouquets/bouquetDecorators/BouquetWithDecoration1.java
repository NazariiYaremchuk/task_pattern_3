package com.yaremchuk.model.bouquets.bouquetDecorators;

import com.yaremchuk.model.bouquets.Bouquet;

public class BouquetWithDecoration1 implements Bouquet {
    Bouquet bouquet;
    int additionalPrice;


    BouquetWithDecoration1(Bouquet bouquet) {
        this.bouquet = bouquet;
        additionalPrice = bouquet.getFlowersCount() * 5;
    }

    @Override
    public int getPrice() {
        return bouquet.getPrice() + additionalPrice;
    }

    @Override
    public int getFlowersCount() {
        return bouquet.getFlowersCount();
    }
}
package com.yaremchuk.model.bouquets;

public interface Bouquet {
    int getPrice();

    int getFlowersCount();
}
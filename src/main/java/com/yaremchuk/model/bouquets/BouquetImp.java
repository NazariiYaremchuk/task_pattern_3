package com.yaremchuk.model.bouquets;

import com.yaremchuk.FlowerType;

public class BouquetImp {
    int price;
    int flowersCount;
    FlowerType type;

    BouquetImp(int flowersCount, FlowerType type) {
        this.flowersCount = flowersCount;
        this.type = type;

    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public int getFlowersCount() {
        return flowersCount;
    }
}

package com.yaremchuk.model.events;

import com.yaremchuk.model.bouquets.Bouquet;

public interface Event {
    Bouquet createBouquet();
}

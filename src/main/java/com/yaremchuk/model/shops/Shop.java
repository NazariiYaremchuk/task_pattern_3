package com.yaremchuk.model.shops;

import java.util.ArrayList;
import java.util.List;

public class Shop {
    String name;
    List<User> users;

    Shop(String name) {
        this.name = name;
        users = new ArrayList<>();
    }

    public void notifyUsers() {
        users.stream().forEach(user -> user.update("Method update had been called!"));
    }

    public void addUser(User user) {
        users.add(user);
    }

    public void removeUser(String name) {
        users.stream().forEach(user -> {
            if (user.getName().equals(name)) users.remove(user);
        });
    }

}
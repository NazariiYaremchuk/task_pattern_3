package com.yaremchuk.model.shops;

public class User {
    private String name;
    private int balance;

    User(String name) {
        this.name = name;
        balance = 0;
    }

    public void update(String message) {
        System.out.println("User " + name + "received message: " + message + "\n");
    }

    public String getName() {
        return name;
    }
}
